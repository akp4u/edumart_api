<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';
require_once('src/OAuth2/Autoloader.php');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class School extends REST_Controller {

    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: authorization, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        /*$method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();*/
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        
        $this->load->helper('common_helper');
        $this->load->model('school_model');
        $this->load->config('serverconfig');


        $dsn        = 'mysql:dbname=' . $this->config->item('oauth_db_database') . ';host=' . $this->config->item('oauth_db_host');
        $dbusername = $this->config->item('oauth_db_username');
        $dbpassword = $this->config->item('oauth_db_password');

        OAuth2\Autoloader::register();
        
        // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
        $storage = new OAuth2\Storage\Pdo(array(
            'dsn' => $dsn,
            'username' => $dbusername,
            'password' => $dbpassword
        ));
        
        // Pass a storage object or array of storage objects to the OAuth2 server class
        $this->oauth_server = new OAuth2\Server($storage);
        
        // Add the "Client Credentials" grant type (it is the simplest of the grant types)
        $this->oauth_server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
        
        // Add the "Authorization Code" grant type (this is where the oauth magic happens)
        $this->oauth_server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        
    }

    public function users_post()
    {
		$this->load->helper('common_helper');
 		$result_arr    = array(); 

            $status                = 1;
            $success_message       = "successful fetched result";
            $result_arr            =  'hii';
            $http_response         = 'http_response_ok';

   
        json_response($status, $result_arr, $http_response, "Error Retrieving", $success_message);

        }


        public function call_amit_post(){

   // echo "hi";die;
        $error_message = $success_message = $status= "";
    $result_arr    = array();

    $test_data = $this->school_model->test_amit();

    if($test_data){

            $status                = 1;
            $success_message       = "successful fetched result";
            $result_arr            =  $test_data;
            $http_response         = 'http_response_ok';

    }
    else{
        $status        = 0;
        $http_response = 'http_response_not_found';
        $error_message = 'No result found';
    }

        json_response($status, $result_arr, $http_response, $error_message, $success_message);
}


public function token_post()
    {
        
        $this->oauth_server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    }
    public function fetch_school_post()
    {

        $result_arr    = array();
        $error_message = $success_message = $status= "";
        
        if (!$this->oauth_server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {


            $error_message = 'Invalid Token';
            $http_response = 'http_response_unauthorized';
            
        } else {
            $data = array();
            $flag = 0;
            /*if (!$this->post('pass_key')) { 
            //$error_message = "Pass key is required";
            $flag = $flag + 1;
            } else {
            $pass_key = $this->post('pass_key', TRUE);
            $data['pass_key'] = $pass_key; //$this->encryption->decrypt($pass_key);
            }
            
            if (!$this->post('user_id')) {
            //$flag = $flag + 1;
            } else {
            $data['user_id'] = $this->post('user_id', TRUE);
            }*/
            
            if (!$this->post('lat')) {
                //$error_message = "Pass key is required";
                $flag = $flag + 1;
            } else {
                $prams['lat'] = $this->post('lat', TRUE);
                
            }
            if (!$this->post('lng')) {
                //$error_message = "Pass key is required";
                $flag = $flag + 1;
            } else {
                $prams['lng'] = $this->post('lng', TRUE);
                
            }
            if (!$this->post('rad')) {
                //$error_message = "Pass key is required";
                $flag = $flag + 1;
            } else {
                $prams['rad'] = $this->post('rad', TRUE);
            }
            
            if ($flag == 0)
                $result['list'] = $this->school_model->getAllSchool($prams);
            // echo $this->db->last_query(); exit;
            // print_r($result);die;
            
            if (!empty($result['list'])) {
                $status                = 1;
                $success_message       = "successful fetched result";
                $result_arr['dataset'] = $result['list'];
                $http_response         = 'http_response_ok';
                /*                        $response['message'] ="Your school details";
                 */
            } else {
               // $http_response = 'http_response_ok_no_content';
                $status        = 0;
                $http_response = 'http_response_ok';
                $error_message = 'No result found';
            }
            /* } 
            else 
            { 
            $http_response      = 'http_response_invaliduser';
            $error_message      = 'User is not valid'; 
            }*/
        }
        /* else 
        { 
        $http_response      = 'http_response_bad_request';
        $error_message      = 'Invalid Parameter'; 
        }*/
        
        json_response($status, $result_arr, $http_response, $error_message, $success_message);
    }

}
